HEAD=openapi/head

# root must be first
NODE=openapi/nodes/root     \
     openapi/nodes/search   \
     openapi/nodes/admins   \
     openapi/nodes/aliases  \
     openapi/nodes/clone    \
     openapi/nodes/comment  \
     openapi/nodes/custom   \
     openapi/nodes/groups   \
     openapi/nodes/location \
     openapi/nodes/room     \
     openapi/nodes/state    \
     openapi/nodes/users

NETWORK=openapi/networks/root   \
        openapi/networks/search

DOMAIN=openapi/domains/root               \
       openapi/domains/search             \
       openapi/domains/admins             \
       openapi/domains/comment            \
       openapi/domains/delegated_flag     \
       openapi/domains/ds_records         \
       openapi/domains/groups             \
       openapi/domains/name               \
       openapi/domains/nameservers        \
       openapi/domains/use_as_name_groups \
       openapi/domains/use_domain_groups

TXT_RECORD=openapi/txt-records/root    \
           openapi/txt-records/search  \
           openapi/txt-records/admins  \
           openapi/txt-records/aliases \
           openapi/txt-records/comment \
           openapi/txt-records/groups  \
           openapi/txt-records/name    \
           openapi/txt-records/values

USER=openapi/users/root   \
     openapi/users/search

ADMIN_TEAM=openapi/admin-teams/root   \
           openapi/admin-teams/search

LOCATION=openapi/locations/root \
         openapi/locations/name \
         openapi/locations/site \
         openapi/locations/code

# keyword must be first
COMPONENTS=openapi/components/keyword       \
           openapi/components/security      \
           openapi/components/schemas       \
           openapi/components/parameters    \
           openapi/components/requestBodies \
           openapi/components/responses

OPENAPI_PARTS = $(HEAD) $(NODE) $(DOMAIN) $(USER) $(NETWORK) $(TXT_RECORD) $(ADMIN_TEAM) $(LOCATION) $(COMPONENTS)

AFS_DIR=/afs/ir.stanford.edu/group/networking/www/netdb/web-service/docs
TNG_AFS_OPENAPI  = $(AFS_DIR)/netdb-tng.openapi
TNG_AFS_HTML     = $(AFS_DIR)/netdb-tng-api.html
DEV_AFS_OPENAPI  = $(AFS_DIR)/netdb-dev.openapi
DEV_AFS_HTML     = $(AFS_DIR)/netdb-dev-api.html
PROD_AFS_OPENAPI = $(AFS_DIR)/netdb.openapi
PROD_AFS_HTML    = $(AFS_DIR)/netdb-api.html

OPENAPI_FILES = template.openapi \
                netdb.openapi \
                netdb-dev.openapi \
                netdb-tng.openapi \
                netdb-local.openapi

HTML_FILES = index.html \
             netdb-dev-api.html \
             netdb-tng-api.html \
             netdb-local.html

VELOCITY_FILES = openapi/velocity/base_routes.tmpl \
                 openapi/velocity/search.tmpl \
                 openapi/velocity/single_value_attribute.tmpl \
                 openapi/velocity/multi_value_attribute.tmpl \
                 openapi/velocity/response_codes.tmpl \
                 openapi/velocity/tag_list.tmpl

NETDB_JAR=../../../../classes/netdb.jar

all: $(OPENAPI_FILES) $(HTML_FILES)

template.openapi: $(OPENAPI_PARTS) $(NETDB_JAR)
	rm -f $@
	cat $(OPENAPI_PARTS) >> $@

netdb.openapi: template.openapi openapi/velocity/context.prod $(VELOCITY_FILES)
	java -cp $(NETDB_JAR):openapi/velocity stanford.netdb.Velocity_Parser -context openapi/velocity/context.prod -template $< > $@

netdb-dev.openapi: template.openapi
	java -cp $(NETDB_JAR):openapi/velocity stanford.netdb.Velocity_Parser -context openapi/velocity/context.dev -template $< > $@

netdb-tng.openapi: template.openapi
	java -cp $(NETDB_JAR):openapi/velocity stanford.netdb.Velocity_Parser -context openapi/velocity/context.tng -template $< > $@

netdb-local.openapi: netdb-tng.openapi
	cp $< $@
	perl -pi -e 's/title: NetDB-tng/title: NetDB local/' netdb-local.openapi
	perl -pi -e 's!https://netdb-tng-api.stanford.edu!http://127.0.0.1:4567!' netdb-local.openapi

index.html:
	ln -s netdb-api.html index.html

netdb-tng-api.html: netdb-api.html
	cp $< $@
	perl -pi -e 's/<title>NetDB /<title>NetDB-tng /i' $@
	perl -pi -e 's/netdb.openapi/netdb-tng.openapi/i' $@

netdb-dev-api.html: netdb-api.html
	cp $< $@
	perl -pi -e 's/<title>NetDB /<title>NetDB-dev /i' $@
	perl -pi -e 's/netdb.openapi/netdb-dev.openapi/i' $@

netdb-local.html: netdb-api.html
	cp $< $@
	perl -pi -e 's/<title>NetDB /<title>NetDB-local /i' $@
	perl -pi -e 's/netdb.openapi/netdb-local.openapi/i' $@

install_tng_in_afs: $(TNG_AFS_OPENAPI) $(TNG_AFS_HTML)
$(TNG_AFS_OPENAPI): netdb-tng.openapi
	cp $< $@

$(TNG_AFS_HTML): netdb-tng-api.html
	cp $< $@

install_dev_in_afs: $(DEV_AFS_OPENAPI) $(DEV_AFS_HTML)
$(DEV_AFS_OPENAPI): netdb-dev.openapi
	cp $< $@

$(DEV_AFS_HTML): netdb-dev-api.html
	cp $< $@

install_prod_in_afs: $(PROD_AFS_OPENAPI) $(PROD_AFS_HTML)
$(PROD_AFS_OPENAPI): netdb.openapi
	cp $< $@

$(PROD_AFS_HTML): netdb-api.html
	cp $< $@

clean:
	rm -f $(OPENAPI_FILES) $(HTML_FILES)

.PHONY: all clean install_tng_in_afs install_prod_in_afs install_dev_in_afs
