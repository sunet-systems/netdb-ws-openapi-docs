
  requestBodies:

    # # # # # #
    # records #
    # # # # # #

    name:
      schema:
        type: object
        properties:
          name: { type: string }

    site:
      schema:
        type: object
        properties:
          site: { type: string }

    code:
      type: object
      properties:
        code: { type: string }

    # # # # # # # #
    # attributes  #
    # # # # # # # #

    admins:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/admins'

    admins_payload:
      type: object
      properties:
        administrators:
          type: object
          properties:
            persons:
              type: array
              items:
                type: object
                properties:
                  netid: { type: string }
                  registry id: { type: string }
            admin teams:
              type: array
              items:
                type: object
                properties:
                  name: { type: string }

    admins_payload_alternate:
      type: object
      properties:
        administrators:
          type: object
          properties:
            admin teams:
              type: array
              items:
                description: A list of admin-team names.
                type: string
            persons:
              type: array
              items:
                description: A list of netid, or registry IDs.
                type: string

    aliases:
      description: A string, or an array of strings are also accepted.
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/aliases'

    custom-fields:
      content:
        application/json:
          schema:
            type: object
            properties:
              custom fields:
                $ref: '#/components/schemas/custom_fields'

    custom-fields_delete:
      description: >
        List of custom field 'labels' to delete.  A string, or an array of strings are also
        accepted.
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              custom fields:
                type: array
                items:
                  type: string

    custom-fields_value:
      description: The 'value' that should be saved with the custom field {label}.
      required: false
      content:
        application/json:
          schema:
            type: string

    comment:
      content:
        application/json:
          schema:
            type: object
            properties:
              comment:
                type: string

    delegated:
      content:
        application/json:
          schema:
            type: object
            properties:
              delegated flag:
                type: boolean

    ds-records:
      content:
        application/json:
          schema:
            type: object
            properties:
              ds records:
                $ref: '#/components/schemas/ds_records'

    expiration-date:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/expiration-date'

    groups:
      description: A string, or an array of strings are also accepted.
      content:
        application/json:
          schema:
            properties:
              groups:
                type: array
                items:
                  type: string

    location-input:
      content:
        application/json:
          schema:
            type: object
            properties:
              location:
                $ref: '#/components/schemas/location-input'

    nameservers:
      description: A string, or an array of strings are also accepted.
      content:
        application/json:
          schema:
            properties:
              nameservers:
                type: array
                items:
                  type: string

    node_users:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/node_users'

    node_users_payload:
      description: >
        Only the netid, or registry ID is needed.  A string, or an array of strings are also
        accepted.
      content:
        application/json:
          schema:
            type: object
            properties:
              users:
                type: array
                items:
                  type: object
                  properties:
                    netid: { type: string }
                    registry id: { type: string }

    node_users_payload_alternate:
      description: A list of netids, or registry IDs.
      content:
        application/json:
          schema:
            properties:
              users:
                type: array
                items:
                  type: string

    room:
      content:
        application/json:
          schema:
            type: object
            properties:
              room:
                type: string

    state:
      content:
        application/json:
          schema:
            type: object
            properties:
              state:
                type: string

    txt-values:
      content:
        application/json:
          schema:
            type: object
            properties:
              values:
                $ref: '#/components/schemas/txt-values_input'

    txt-values_delete:
      description: List of values to delete.  A string, or an array of strings are also accepted.
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              values:
                type: array
                items:
                  type: string

    txt-value-value:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/txt-value-value'

    use_as_name_groups:
      description: A string, or an array of strings are also accepted.
      content:
        application/json:
          schema:
            properties:
              use as name groups:
                type: array
                items:
                  type: string

    use_domain_groups:
      description: A string, or an array of strings are also accepted.
      content:
        application/json:
          schema:
            properties:
              use domain groups:
                type: array
                items:
                  type: string

