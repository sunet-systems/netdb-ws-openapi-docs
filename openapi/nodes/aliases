
  /nodes/{node_name}/aliases:
    get:
      operationId: get node aliases
      summary: Get the aliases on {node_name}.
      description: >
        {node_name} must be a name that can have aliases.  Using an different type of name (for
        instance an alias, or an MX) will result in an error, even though a record may be found.
      parameters:
        - $ref: '#/components/parameters/alias_name'
      responses:
        200:
          $ref: '#/components/responses/200-alias'
        400:
          $ref: '#/components/responses/400-node-alias'
        404:
          $ref: '#/components/responses/404-node'
      tags: [node aliases]

    post:
      operationId: add node aliases
      summary: >
        Add the aliases to {node_name}.  The payload provided will be added to the aliases
        already on {node_name}.
      description: >
        {node_name} must be a name that can have aliases.  Using an different type of name (for
        instance an alias, or an MX) will result in an error, even though a record may be found.
        <br><br>
        If the payload contains aliases that exist on {node_name}, the operation will be
        considered a success.  The server interprets a post request as 'the user wants the data
        in the payload on {node_name}'.  The end result is that the payload is there, even though
        it was a no-op on the server's end.
      parameters:
        - $ref: '#/components/parameters/alias_name'
        - $ref: '#/components/parameters/aliases_query'
      requestBody:
        $ref: '#/components/requestBodies/aliases'
      responses:
        200:
          $ref: '#/components/responses/200-alias'
        400:
          $ref: '#/components/responses/400-node-alias'
        403:
          $ref: '#/components/responses/403-node'
        404:
          $ref: '#/components/responses/404-node'
      tags: [node aliases]

    put:
      operationId: set node aliases
      summary: >
        Set the aliases for {node_name}.  The payload provided will be the only aliases on
        {node_name} after this operation.
      description: >
        {node_name} must be a name that can have aliases.  Using an different type of name (for
        instance an alias, or an MX) will result in an error, even though a record may be found.
      parameters:
        - $ref: '#/components/parameters/alias_name'
        - $ref: '#/components/parameters/aliases_query'
      requestBody:
        $ref: '#/components/requestBodies/aliases'
      responses:
        200:
          $ref: '#/components/responses/200-alias'
        400:
          $ref: '#/components/responses/400-node-alias'
        403:
          $ref: '#/components/responses/403-node'
        404:
          $ref: '#/components/responses/404-node'
      tags: [node aliases]

    delete:
      operationId: delete node aliases
      summary: Remove aliases from {node_name}.
      description: >
        {node_name} must be a name that can have aliases.  Using an different type of name (for
        instance an alias, or an MX) will result in an error, even though a record may be found.
        <br><br>
        If the payload contains aliases that don't exist on {node_name}, the operation will be
        considered a success.  The server interprets a delete request as 'the user doesn't want
        the data in the payload on {node_name}'.  The end result is that the payload isn't there,
        even though it was a no-op on the server's end.
      parameters:
        - $ref: '#/components/parameters/alias_name'
        - $ref: '#/components/parameters/aliases_query'
      requestBody:
        $ref: '#/components/requestBodies/aliases'
      responses:
        200:
          $ref: '#/components/responses/200-alias'
        400:
          $ref: '#/components/responses/400-node-alias'
        403:
          $ref: '#/components/responses/403-node'
        404:
          $ref: '#/components/responses/404-node'
      tags: [node aliases]
